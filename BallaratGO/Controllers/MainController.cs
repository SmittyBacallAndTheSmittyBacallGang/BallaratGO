﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BallaratGO.Models;
using BallaratGO.Models.Ballarat;
using Microsoft.AspNet.Identity;

namespace BallaratGO.Controllers
{
    [Authorize]
    public class MainController : ApiController
    {
        // GET api/<controller>
        public GoData Get(double lon, double lat)
        {
            //http://localhost:61786/api/main?lon=0&lat=0
            using (ApplicationDbContext model = new ApplicationDbContext())
            {
                var queriedLocation = DbGeography.FromText("POINT(" + lon + " " + lat + ")");

                var goData = new GoData();

                Debug.WriteLine(model.DrinkingFountains.First().location.AsText());

                goData.NearbyLocations.AddRange(model.DrinkingFountains.Where(i => i.location.Distance(queriedLocation) <= 50).Select(i=> new GoData.NearbyLocation() { Distance = i.location.Distance(queriedLocation).Value, ID = i.ID, Name = i.Area }));
                goData.NearbyLocations.AddRange(model.ArtWorks.Where(i => i.location.Distance(queriedLocation) <= 50).Select(i => new GoData.NearbyLocation() { Distance = i.location.Distance(queriedLocation).Value, ID = i.ID, Name = i.LocationName + " " + i.FeatureType.Replace("AH-", "") }));
                goData.NearbyLocations.AddRange(model.Playgrounds.Where(i => i.location.Distance(queriedLocation) <= 50).Select(i => new GoData.NearbyLocation() { Distance = i.location.Distance(queriedLocation).Value, ID = i.ID, Name = i.Site }));

                goData.DistantLocations.Add(GetStuff(queriedLocation, model.DrinkingFountains.Select(i=>new POI() {ID = i.ID, location = i.location}), 50, 250));
                goData.DistantLocations.Add(GetStuff(queriedLocation, model.ArtWorks.Select(i => new POI() { ID = i.ID, location = i.location }), 50, 250));
                goData.DistantLocations.Add(GetStuff(queriedLocation, model.Playgrounds.Select(i => new POI() { ID = i.ID, location = i.location }), 50, 250));

                //User.Identity.GetUserId();
                return goData;
            }
            
        }

        private GoData.DistantLocation GetStuff(DbGeography queriedLocation, IEnumerable<POI> poi, double innerThreshold, double outerThreshold)
        {
            const int areaSegment = 8;

            var segmentPoints = new List<List<DbGeography>>(); // 

            for (var j = 0; j < areaSegment; j++)
            {
                segmentPoints.Insert(j, new List<DbGeography>());
            }
            

            // Step 1: Retrieve Points from the database, which points are within the searching area. 
            var rawpoints = poi.Where(p => p.location.Distance(queriedLocation) > innerThreshold && p.location.Distance(queriedLocation) < outerThreshold).ToList();

            // Step 2: Convert raw-data to string list and divide points into 8 segments.
            foreach (var t in rawpoints)
            {
                segmentPoints[CategoriseRegion(t.location, queriedLocation)].Add(DbGeography.FromText(t.location.ProviderValue.ToString()));
            }
            //var test = model.GPSPoints.Where(p => p.Location.Equals(DbGeography.FromText("POINT (142.75 -36.9)")));

            if (segmentPoints.All(i => !i.Any())) return new GoData.DistantLocation();

            // Step 3: Find median value of segments.
            var numPointsOfSegment = segmentPoints.Select(t => t?.Count ?? 0).ToList();

            var medianS = GetMedian(numPointsOfSegment);

            var segmentNumber = segmentPoints.First(sp => sp.Count == medianS);
            //Console.WriteLine(Segmentpoints.IndexOf(segmentNumber));//[testing only]

            // Step 4: Find central
            var targetCentrePoint = FindCentrePoint(segmentPoints[segmentPoints.IndexOf(segmentNumber)]);

            // Step 5: Distance and angle
            var targetDistance = double.Parse(queriedLocation.Distance(targetCentrePoint).ToString());
            var targetAngle = GetAngle(targetCentrePoint, queriedLocation);
            //Console.WriteLine("[DBG MSG]Distance: " + targetDistance + "\t Angle:" + targetAngle);// [Testing only]

            return new GoData.DistantLocation()
            {
                Angle = targetAngle,
                Distance = targetDistance,
                POICount = numPointsOfSegment.Count()
            };
        }

        // Search the region that location belong with
        public static int CategoriseRegion(DbGeography location, DbGeography targetCentrePoint)
        {
            ////Step1: calculator the angle (45 degree each region)
            var angle = GetAngle(location, targetCentrePoint);

            //Step2: Check condition and return the region number.
            if (angle <= 45.0)  //Region 0
                return 0;
            else if (45.0 < angle && angle <= 90.0) //Region 1
                return 1;
            else if (90.0 < angle && angle <= 135.0)    //Region 2
                return 2;
            else if (135.0 < angle && angle <= 180.0)   //Region 3
                return 3;
            else if (180.0 < angle && angle <= 225.0)   //Region 4
                return 4;
            else if (225.0 < angle && angle <= 270.0)   //Region 5
                return 5;
            else if (270.0 < angle && angle <= 315.0)   //Region 6
                return 6;
            else   //Region 7  
                return 7;
        }

        // GetAngle()
        static double GetAngle(DbGeography location, DbGeography targetCentrePoint)
        {
            var calVar = new double[3]; // Variables for angle calculation
            //Step1: calculator the angle (45 degree each region)
            calVar[0] = double.Parse(location.Latitude.ToString()) - double.Parse(targetCentrePoint.Latitude.ToString());//CentreLong - RefLong;
            calVar[1] = double.Parse(location.Longitude.ToString()) - double.Parse(targetCentrePoint.Longitude.ToString()); //CentreLa - RefLa;
            calVar[2] = Math.Atan2(calVar[0], calVar[1]);
            var angle = (90 - ((calVar[2] * 180) / Math.PI)) % 360;

            //Note: Prevent minor value
            if (angle < 0)
                angle = angle + 360;
            return angle;
        }

        // Calculate median - input number of count of each segment to get the value
        static int GetMedian(List<int> numPointOfSegment)
        {
            numPointOfSegment.Sort();
            List<int> localNumPoint = numPointOfSegment.Distinct().ToList();
            return localNumPoint[localNumPoint.Count() / 2];
        }

        //
        static DbGeography FindCentrePoint(List<DbGeography> segmentPoints)
        {
            var longtitude = new List<double>();
            var latitude = new List<double>();
            double x = 0, y = 0;
            DbGeography centrePoint = null;
            foreach (var t in segmentPoints)
            {
                longtitude.Add(double.Parse(t.Longitude.ToString()));
                latitude.Add(double.Parse(t.Latitude.ToString()));
                //Console.WriteLine(Segmentpoints[i].Longitude.ToString()+ Segmentpoints[i].Latitude.ToString()); // [Testing only]
            }
            for (var j = 0; j < segmentPoints.Count; j++)
            {
                x += longtitude[j];
                y += latitude[j];
            }
            x /= segmentPoints.Count;
            y /= segmentPoints.Count;
            centrePoint = DbGeography.FromText("POINT(" + x + " " + y + ")");
            return centrePoint;
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}