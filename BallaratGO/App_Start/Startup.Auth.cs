﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Owin;
using BallaratGO.Models;
using RestSharp;
using BallaratGO.Models.DataImports;
using BallaratGO.Models.Ballarat;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Data.Entity.Spatial;
using System.Xml.Linq;

namespace BallaratGO
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

      
            using (ApplicationDbContext dbContext = new ApplicationDbContext()) {
                // Water Fountains
                //importWaterFountainJson("https://data.gov.au/dataset/0c41aa28-5d9c-48f5-b4a7-5eecd8f90c8c/resource/7be3e3aa-0f01-4c10-bdc4-ccdebb2cd06d/download/BallaratDrinkingFountains.geojson", dbContext);
                // Art Locations
                //importArtWorksXml("http://data.gov.au/dataset/4b112645-dcc3-4d55-a0f8-b900ef1b074d/resource/4bfce7af-4dfb-4750-94b6-cc32f486aa8d/download/artwork.kml", dbContext);
                // Playground Locations
                importPlayGrounds("http://data.gov.au/dataset/a9b248c1-2078-45fa-b9c6-b2ae562c87b2/resource/693b8663-efd6-4583-9dd6-7a3793e54bae/download/BallaratPlaygrounds.geojson", dbContext);
                // Reserves 
                //importGraffitiJson("http://data.gov.au/geoserver/ballarat-reserves/wfs?request=GetFeature&typeName=ckan_75910b1c_d605_4484_a603_807942a5611e&outputFormat=json", dbContext);
                // Graffiti
                //importReservesJson("http://data.gov.au/geoserver/ballarat-graffiti-defects/wfs?request=GetFeature&typeName=98777953_2467_49e3_bd40_0d334aed10ea&outputFormat=json", dbContext);
            }
        }

        // Update the Artwork location information into Database
        private void importArtWorksXml(string x, ApplicationDbContext model)
        {
            XDocument xdoc = XDocument.Load(x);

            var ns = XNamespace.Get("http://www.opengis.net/kml/2.2");
            var placemarks = xdoc.Element(ns + "kml").Element(ns + "Document").Elements(ns + "Placemark");

            foreach (var placemark in placemarks)
            {
                var artwork = new ArtWork();
                string[] coord = new string[3];

                artwork.Site = (placemark.Element(ns+ "name") == null ? null : placemark.Element(ns + "name").Value);
                artwork.ExternalID = placemark.Element(ns + "ExtendedData").Element(ns + "SchemaData").Elements(ns + "SimpleData")?.SingleOrDefault(i => i.Attributes().Any(j => j.Name == "name" && j.Value == "ID"))?.Value;
                artwork.Suburb = placemark.Element(ns + "ExtendedData").Element(ns + "SchemaData").Elements(ns + "SimpleData")?.SingleOrDefault(i => i.Attributes().Any(j => j.Name == "name" && j.Value == "SUBURB"))?.Value;
                artwork.FeatureType = placemark.Element(ns + "ExtendedData").Element(ns + "SchemaData").Elements(ns + "SimpleData")?.SingleOrDefault(i => i.Attributes().Any(j => j.Name == "name" && j.Value == "FEAT_TYPE"))?.Value;
                artwork.LocationName = placemark.Element(ns + "ExtendedData").Element(ns + "SchemaData").Elements(ns + "SimpleData")?.SingleOrDefault(i => i.Attributes().Any(j => j.Name == "name" && j.Value == "LOC_NAME"))?.Value;
                coord = placemark.Element(ns + "Point").Element(ns + "coordinates").Value.Split(',');
                artwork.location = DbGeography.FromText($"POINT({coord[0]} {coord[1]})"); 
                model.ArtWorks.AddOrUpdate(i => i.ExternalID, artwork);
            }

            model.SaveChanges();

        }

        // Update the Playgroud location information into Database
        private static void importPlayGrounds(string url, ApplicationDbContext model)
        {
            var client = new RestClient(url);
            var request = new RestRequest();
            var response = client.Execute<Models.DataImports.Playground.RootObject>(request);

            var features = response.Data.features.Where(i => i.geometry != null && i.geometry.@type != null && i.geometry.@type == "Point");

            foreach (var feature in features)
            {
                var playground = new Playground();

                playground.ExternalID = feature.properties.ID;
                playground.Suburb = feature.properties.Area;
                playground.LocationName = feature.properties.Location;
                playground.FeatureType = feature.properties.PlayType;
                playground.Site = feature.properties.Site;
                playground.location = DbGeography.FromText($"POINT({feature.geometry.coordinates.First()} {feature.geometry.coordinates.Skip(1).First()})"); //feature.geometry.coordinates;

                model.Playgrounds.AddOrUpdate(i => i.ExternalID, playground);
            }

            model.SaveChanges();
        }

        /* These can all be methods within a matching class placed in the file with namespace. */
        private void importWaterFountainJson(string url, ApplicationDbContext dal)
        {
            var client = new RestClient(url);
            var request = new RestRequest();
            var response = client.Execute<Models.DataImports.DrinkingFountain.RootObject>(request);

            var features = response.Data.features.Where(i => i.geometry.type == "Point");


            foreach (var feature in features)
            {
                var drinkingFountain = new DrinkingFountain();

                drinkingFountain.ExternalID = feature.properties.CentralAssetID;
                drinkingFountain.Area = feature.properties.Area;
                drinkingFountain.location = DbGeography.FromText($"POINT({feature.geometry.coordinates.First()} {feature.geometry.coordinates.Last()})"); //feature.geometry.coordinates;

                dal.DrinkingFountains.AddOrUpdate(i => i.ExternalID, drinkingFountain);
            }

            dal.SaveChanges();
        }       

        private void importReservesJson(string url, ApplicationDbContext dal)
        {
            var client = new RestClient(url);
            var request = new RestRequest();
            var response = client.Execute<Models.DataImports.CouncilReserve.RootObject>(request);

            var features = response.Data.features.Where(i => i.geometry.type == "Point");


            foreach (var feature in features)
            {
                var reserve = new Reserve();

                reserve.ExternalID = feature.properties.id;
                reserve.Site = feature.properties.site;
                reserve.FeatureType = feature.properties.feat_type;
                reserve.location = DbGeography.FromText($"POINT({feature.geometry.coordinates.First()} {feature.geometry.coordinates.Last()})"); //feature.geometry.coordinates;

                dal.Reserves.AddOrUpdate(i => i.ExternalID, reserve);
            }

            dal.SaveChanges();
        }

        private void importGraffitiJson(string url, ApplicationDbContext dal)
        {
            var client = new RestClient(url);
            var request = new RestRequest();
            var response = client.Execute<Models.DataImports.Graffti.RootObject>(request);

            var features = response.Data.features.Where(i => i.geometry.type == "Point");


            foreach (var feature in features)
            {
                var graffiti = new Graffiti();

                graffiti.ExternalID = feature.properties.asset;
                graffiti.Site = feature.properties.site;
                graffiti.location = DbGeography.FromText($"POINT({feature.geometry.coordinates.First()} {feature.geometry.coordinates.Last()})"); //feature.geometry.coordinates;

                dal.Graffitis.AddOrUpdate(i => i.ExternalID, graffiti);
            }

            dal.SaveChanges();
        }
    }

}
