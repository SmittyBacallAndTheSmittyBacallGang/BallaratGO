﻿using System.Collections.Generic;

namespace BallaratGO.Controllers
{
    public class GoData
    {
        public List<NearbyLocation> NearbyLocations { get; set; } = new List<NearbyLocation>();

        public List<DistantLocation> DistantLocations { get; set; } = new List<DistantLocation>();

        public class NearbyLocation
        {
            public double Distance { get; set; }
            public string Name { get; set; }
            public int ID { get; set; }
        }

        public class DistantLocation
        {
            public double Distance { get; set; }
            public double Angle { get; set; } //What is the data type?
            public int POICount { get; set; }
        }
    }
}

/*
 {
    nearby:[
        {
            distance: 1,
            name: "Artwork", //perhaps say what type it is, as in "shop", rather than Coles
            poiID: 1
        }
    ], //these are the locations that are able to be checked in 
distantTarget: [
        {    

        distance: 50,
        angle: 17,
        //epicentreGPS_X: -144,
        //epicentreGPS_Y: 128,
        poiCount: 50
        },
        {    
            distance: 100,
            angle: 17,
            //epicentreGPS_X: -144,
            //epicentreGPS_Y: 128,
            poiCount: 50
        }
    ]
}
     */