﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BallaratGO.Models.Ballarat
{
    public class Graffiti : POI
    {
        public string Asset { get; set; } //is the Id 
        public string Site { get; set; } //d e.g. Albert Street Toilets
        public string ExternalID { get; set; }
    }
}