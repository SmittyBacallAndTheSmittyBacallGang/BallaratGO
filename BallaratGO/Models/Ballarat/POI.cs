﻿using System.Data.Entity.Spatial;

namespace BallaratGO.Models.Ballarat
{
    public class POI
    {
        public int ID { get; set; }
        public DbGeography location { get; set; }
    }
}