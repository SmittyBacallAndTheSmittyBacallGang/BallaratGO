﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BallaratGO.Models.Ballarat
{

    public class Reserve : POI
    {
        public string FeatureType { get; set; }
        public string ExternalID { get; set; }
        public string Site { get; set; }
    }
}