﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BallaratGO.Models.Ballarat
{

    public class ArtWork : POI
    {
        public string FeatureType { get; set; } //e.g. A Statue
        public string LocationName { get; set; } //e.g. Flora (name of the statue)
        public string Suburb { get; set; }

        public string Site { get; set; } //e.g.  Ballarat Botanical Gardens
        public string ExternalID { get; set; }
    }
}