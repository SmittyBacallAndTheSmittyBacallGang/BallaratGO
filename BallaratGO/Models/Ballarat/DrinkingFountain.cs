﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;

namespace BallaratGO.Models.Ballarat
{
    public class DrinkingFountain : POI
    {
        public string Area { get; set; } //looks to be the suburb
        public string ExternalID { get; set; }

        public string SiteName { get; set; } //e.g. Ballarat Botanical Gardens
       /* e.g. Ballarat Botanical Gardens, looks to be the same  as SiteName in the raw data.*/
        public string FeatureLocation { get; set; }
    }
}