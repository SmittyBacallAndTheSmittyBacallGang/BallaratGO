﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BallaratGO.Models.Ballarat
{

    public class Playground : POI
    {
        public string FeatureType { get; set; }
        public string LocationName { get; set; }
        public string Suburb { get; set; }

        public string Site { get; set; } 
        public string ExternalID { get; set; }
    }
}