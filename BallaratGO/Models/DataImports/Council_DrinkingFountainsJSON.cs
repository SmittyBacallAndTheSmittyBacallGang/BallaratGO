﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BallaratGO.Models.DataImports.DrinkingFountain
{
    public class Geometry
    {
        public string type { get; set; }
        public List<double> coordinates { get; set; }
    }

    public class Properties
    {
        public string Area { get; set; }
        public string CentralAssetID { get; set; }
        public string Class { get; set; }
        public string FeatureLocation { get; set; }
        public string Maintaining_Authority { get; set; }
        public string SiteName { get; set; }
        public string Type { get; set; }
        public string Ward { get; set; }
        public string @long { get; set; }
        public string lat { get; set; }
        }

    public class Feature
    {
        public string type { get; set; }
        public Geometry geometry { get; set; }
        public Properties properties { get; set; }
    }

    public class RootObject
    {
        public string name { get; set; }
        public string type { get; set; }
        public List<Feature> features { get; set; }
    }
}
