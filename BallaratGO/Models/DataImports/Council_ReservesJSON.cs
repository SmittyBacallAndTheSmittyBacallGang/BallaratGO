﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BallaratGO.Models.DataImports.CouncilReserve
{

public class Geometry
    {
        public string type { get; set; }
        public List<double> coordinates { get; set; }
    }

    public class Properties
    {
        public string id { get; set; }
        public string site { get; set; }
        public string location { get; set; }
        public string feat_type { get; set; }
        public int easting { get; set; }
        public int northing { get; set; }
        public double measure { get; set; }
        public string unit { get; set; }
    }

    public class Feature
    {
        public string type { get; set; }
        public string id { get; set; }
        public Geometry geometry { get; set; }
        public string geometry_name { get; set; }
        public Properties properties { get; set; }
    }

    public class Properties2
    {
        public string name { get; set; }
    }

    public class Crs
    {
        public string type { get; set; }
        public Properties2 properties { get; set; }
    }

    public class RootObject
    {
        public string type { get; set; }
        public int totalFeatures { get; set; }
        public List<Feature> features { get; set; }
        public Crs crs { get; set; }
    }


}