﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BallaratGO.Models.DataImports.Playground
{

    public class Geometry
    {
        public string type { get; set; }
        public List<double> coordinates { get; set; }
    }

    public class Properties
    {
        public string Area { get; set; }
        public string ID { get; set; }
        public string Location { get; set; }
        public string PlayType { get; set; }
        public string Maintain { get; set; }
        public string Site { get; set; }
        public string Ward { get; set; }
        public string Constructed { get; set; }
    }

    public class Feature
    {
        public string type { get; set; }
        public Geometry geometry { get; set; }
        public Properties properties { get; set; }
    }
    
    public class RootObject
    {
        public string name { get; set; }
        public string type { get; set; }
        public List<Feature> features { get; set; }
    }

}