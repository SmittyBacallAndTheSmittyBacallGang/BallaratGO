var viewModel = null;

$(document).ready(function () {

    createviewModel();

    getLocations();

});

// KNOCKOUT 
function createviewModel() {
    viewModel = {
        locations: ko.observableArray([])
    }
    ko.applyBindings(viewModel);
}

function getLocations() {

    $.ajax({
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        url: '/api/main?lat=-37.5463240144254&lon=143.822694025132',
        dataType: 'json'
    })
    .done(function (data) {
        viewModel.locations(data.d);
    })
    .fail(function (error) {
        console.log(error.responseJSON.ExceptionType + ': ' + error.responseJSON.Message);
        console.log(error.responseJSON.StackTrace);
    });
}



// New DeviceOrientation (Currently only work on iPhone)
// -----------------------------------------------------
// Obtain a new *world-oriented* Full Tilt JS DeviceOrientation Promise
var promise = FULLTILT.getDeviceOrientation({ 'type': 'world' });

// Wait for Promise result
promise.then(function(deviceOrientation) { // Device Orientation Events are supported

    // Register a callback to run every time a new 
    // deviceorientation event is fired by the browser.
    deviceOrientation.listen(function() {

        // Get the current *screen-adjusted* device orientation angles
        var currentOrientation = deviceOrientation.getScreenAdjustedEuler();

        // Calculate the current compass heading that the user is 'looking at' (in degrees)
        var compassHeading = 360 - currentOrientation.alpha;

        // Do something with `compassHeading` here...
        console.log(compassHeading);

        compassHeading = compassHeading * -1;

        document.getElementById("direction2").innerHTML = compassHeading;

        /* First arrow test */
        var test = document.getElementById("test");
        test.style.transform = "rotate(" + compassHeading + "deg)";

    });

}).catch(function(errorMessage) { // Device Orientation Events are not supported

    console.log(errorMessage);

    // Implement some fallback controls here...

});

