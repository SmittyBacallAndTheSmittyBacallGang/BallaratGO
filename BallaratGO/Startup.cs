﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BallaratGO.Startup))]
namespace BallaratGO
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
