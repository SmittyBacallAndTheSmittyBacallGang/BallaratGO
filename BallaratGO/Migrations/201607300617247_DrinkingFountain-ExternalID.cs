namespace BallaratGO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DrinkingFountainExternalID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DrinkingFountains", "ExternalID", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DrinkingFountains", "ExternalID");
        }
    }
}
