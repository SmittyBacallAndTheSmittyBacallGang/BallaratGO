namespace BallaratGO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class More : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DrinkingFountains", "location", c => c.Geography());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DrinkingFountains", "location", c => c.Geometry());
        }
    }
}
