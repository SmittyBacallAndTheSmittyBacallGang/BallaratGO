namespace BallaratGO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class extras : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DrinkingFountains", "SiteName", c => c.String());
            AddColumn("dbo.DrinkingFountains", "SiteLocation", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DrinkingFountains", "SiteLocation");
            DropColumn("dbo.DrinkingFountains", "SiteName");
        }
    }
}
