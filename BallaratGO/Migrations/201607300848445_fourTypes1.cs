namespace BallaratGO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fourTypes1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ArtWorks",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FeatureType = c.String(),
                        LocationName = c.String(),
                        Suburb = c.String(),
                        Site = c.String(),
                        ExternalID = c.String(),
                        location = c.Geography(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Graffiti",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Asset = c.String(),
                        Site = c.String(),
                        ExternalID = c.String(),
                        location = c.Geography(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Reserves",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FeatureType = c.String(),
                        ExternalID = c.String(),
                        Site = c.String(),
                        location = c.Geography(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Reserves");
            DropTable("dbo.Graffiti");
            DropTable("dbo.ArtWorks");
        }
    }
}
