namespace BallaratGO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class playgrounds : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Playgrounds",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FeatureType = c.String(),
                        LocationName = c.String(),
                        Suburb = c.String(),
                        Site = c.String(),
                        ExternalID = c.String(),
                        location = c.Geography(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Playgrounds");
        }
    }
}
