namespace BallaratGO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fourTypes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DrinkingFountains", "FeatureLocation", c => c.String());
            DropColumn("dbo.DrinkingFountains", "SiteLocation");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DrinkingFountains", "SiteLocation", c => c.String());
            DropColumn("dbo.DrinkingFountains", "FeatureLocation");
        }
    }
}
