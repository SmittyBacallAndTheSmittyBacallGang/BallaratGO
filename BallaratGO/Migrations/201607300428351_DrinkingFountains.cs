namespace BallaratGO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DrinkingFountains : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DrinkingFountains",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Area = c.String(),
                        location = c.Geography(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.DrinkingFountains");
        }
    }
}
